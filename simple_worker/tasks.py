import time
from celery import Celery
from celery.utils.log import get_task_logger
import os
import psycopg2

os.environ.setdefault('FORKED_BY_MULTIPROCESSING', '1')
conn = psycopg2.connect(
    database="hello_flask_dev",
    user="hello_flask",
    password="hello_flask",
    host='db'
)
# creating a demo table in postgresql
cur = conn.cursor()
try:
    cur.execute("CREATE TABLE test (id serial PRIMARY KEY, data varchar);")
except:
    print("Can't create the table")
conn.commit()

# adding a dummy data to query it later through api
try:
    postgres_insert_query = """ INSERT INTO test (id, data) VALUES (%s,%s)"""
    record_to_insert = (1, 'Coda Payments')
    cur.execute(postgres_insert_query, record_to_insert)
except:
    print("Failed to insert record")
conn.commit()

logger = get_task_logger(__name__)

# app = Celery('tasks', broker='redis://localhost:6379/0',
#              backend='redis://localhost:6379/0')
# in case of testing in local machine, use above line and comment below one
app = Celery('tasks', broker='redis://redis:6379/0',
             backend='redis://redis:6379/0')


@app.task()
def longtime_add():
    logger.info('Got Request - Starting work ')
    cur.execute("SELECT * FROM test")
    ans = []
    rows = cur.fetchall()
    for row in rows:
        ans.append(row)
    time.sleep(5)
    logger.info('Work Finished ')
    return ans
