from flask import Flask, jsonify
from celery import Celery


app = Flask(__name__)
simple_app = Celery(
    'simple_worker', broker='redis://redis:6379/0', backend='redis://redis:6379/0')

# for testing on local machine, use below command and comment the above line
# simple_app = Celery(
#     'simple_worker', broker='redis://localhost:6379/0', backend='redis://localhost:6379/0')

# bitbucket app password XuCNSHbtYtzvfKC2d36W


@app.route('/start_task')
def call_method():
    app.logger.info("Invoking Method ")
    r = simple_app.send_task('tasks.longtime_add')
    app.logger.info(r.backend)
    return r.id


@app.route('/task_status/<task_id>')
def get_status(task_id):
    status = simple_app.AsyncResult(task_id, app=simple_app)
    print("Invoking Method ")
    return "Status of the Task " + str(status.state)


@app.route('/task_result/<task_id>')
def task_result(task_id):
    result = simple_app.AsyncResult(task_id).result
    return jsonify({"output": result})
